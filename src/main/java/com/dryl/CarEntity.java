package com.dryl;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car", schema = "MySixtDB")
public class CarEntity {

  private Integer id;
  private Integer needUserAge;
  private String carNumber;
  private Byte inRent;
  private Integer rentCost;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "need_user_age")
  public Integer getNeedUserAge() {
    return needUserAge;
  }

  public void setNeedUserAge(Integer needUserAge) {
    this.needUserAge = needUserAge;
  }

  @Basic
  @Column(name = "car_number")
  public String getCarNumber() {
    return carNumber;
  }

  public void setCarNumber(String carNumber) {
    this.carNumber = carNumber;
  }

  @Basic
  @Column(name = "in_rent")
  public Byte getInRent() {
    return inRent;
  }

  public void setInRent(Byte inRent) {
    this.inRent = inRent;
  }

  @Basic
  @Column(name = "rent_cost")
  public Integer getRentCost() {
    return rentCost;
  }

  public void setRentCost(Integer rentCost) {
    this.rentCost = rentCost;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CarEntity carEntity = (CarEntity) o;

    if (id != null ? !id.equals(carEntity.id) : carEntity.id != null) {
      return false;
    }
    if (needUserAge != null ? !needUserAge.equals(carEntity.needUserAge)
        : carEntity.needUserAge != null) {
      return false;
    }
    if (carNumber != null ? !carNumber.equals(carEntity.carNumber) : carEntity.carNumber != null) {
      return false;
    }
    if (inRent != null ? !inRent.equals(carEntity.inRent) : carEntity.inRent != null) {
      return false;
    }
    if (rentCost != null ? !rentCost.equals(carEntity.rentCost) : carEntity.rentCost != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (needUserAge != null ? needUserAge.hashCode() : 0);
    result = 31 * result + (carNumber != null ? carNumber.hashCode() : 0);
    result = 31 * result + (inRent != null ? inRent.hashCode() : 0);
    result = 31 * result + (rentCost != null ? rentCost.hashCode() : 0);
    return result;
  }
}
