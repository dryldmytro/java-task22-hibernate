package com.dryl;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comment", schema = "MySixtDB", catalog = "")
public class CommentEntity {

  private Integer id;
  private String textComment;
  private Timestamp timeComment;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "text_comment")
  public String getTextComment() {
    return textComment;
  }

  public void setTextComment(String textComment) {
    this.textComment = textComment;
  }

  @Basic
  @Column(name = "time_comment")
  public Timestamp getTimeComment() {
    return timeComment;
  }

  public void setTimeComment(Timestamp timeComment) {
    this.timeComment = timeComment;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CommentEntity that = (CommentEntity) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (textComment != null ? !textComment.equals(that.textComment) : that.textComment != null) {
      return false;
    }
    if (timeComment != null ? !timeComment.equals(that.timeComment) : that.timeComment != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (textComment != null ? textComment.hashCode() : 0);
    result = 31 * result + (timeComment != null ? timeComment.hashCode() : 0);
    return result;
  }
}
