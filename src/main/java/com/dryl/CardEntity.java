package com.dryl;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "card", schema = "MySixtDB")
@IdClass(CardEntityPK.class)
public class CardEntity {

  private Integer cardNumber;
  private Integer cvv;
  private Integer userId;

  @Id
  @Column(name = "card_number")
  public Integer getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(Integer cardNumber) {
    this.cardNumber = cardNumber;
  }

  @Basic
  @Column(name = "cvv")
  public Integer getCvv() {
    return cvv;
  }

  public void setCvv(Integer cvv) {
    this.cvv = cvv;
  }

  @Id
  @Column(name = "user_id")
  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CardEntity that = (CardEntity) o;

    if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null) {
      return false;
    }
    if (cvv != null ? !cvv.equals(that.cvv) : that.cvv != null) {
      return false;
    }
    if (userId != null ? !userId.equals(that.userId) : that.userId != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = cardNumber != null ? cardNumber.hashCode() : 0;
    result = 31 * result + (cvv != null ? cvv.hashCode() : 0);
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    return result;
  }
}
