package com.dryl;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fine", schema = "MySixtDB", catalog = "")
public class FineEntity {

  private Integer id;
  private String fineName;
  private Timestamp date;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "fine_name")
  public String getFineName() {
    return fineName;
  }

  public void setFineName(String fineName) {
    this.fineName = fineName;
  }

  @Basic
  @Column(name = "date")
  public Timestamp getDate() {
    return date;
  }

  public void setDate(Timestamp date) {
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    FineEntity that = (FineEntity) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (fineName != null ? !fineName.equals(that.fineName) : that.fineName != null) {
      return false;
    }
    if (date != null ? !date.equals(that.date) : that.date != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (fineName != null ? fineName.hashCode() : 0);
    result = 31 * result + (date != null ? date.hashCode() : 0);
    return result;
  }
}
