package com.dryl;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class Main {

  private static SessionFactory ourSessionFactory;

  static {
    try {
      ourSessionFactory = new Configuration().configure().buildSessionFactory();
    } catch (Throwable ex) {
      throw new ExceptionInInitializerError(ex);
    }
  }

  public static Session getSession() throws HibernateException {
    return ourSessionFactory.openSession(); //return opened session
  }

  //---------------------------------------------------------------------------
  public static void main(final String[] args) throws Exception {
    // get opened session
    Session session = getSession();
    try {
      ReadAllTable(session);
      System.out.println("Finish work!");
    } finally {
      session.close();
      System.exit(0);
    }
  }

  private static void ReadAllTable(Session session) {

//region Read Person
    Query query = session.createQuery("from " + "UserEntity");
    System.out.format("\nTable Person --------------------\n");
    for (Object obj : query.list()) {
      UserEntity person = (UserEntity) obj;
      System.out.println(person.getId()+" "+
          person.getFirstName()+" " +person.getSecondName()+" "+person.getMobileNumber()+" "+
          person.getLogin());
    }
  }
}
