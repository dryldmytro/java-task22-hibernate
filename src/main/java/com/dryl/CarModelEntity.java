package com.dryl;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car_model", schema = "MySixtDB")
public class CarModelEntity {

  private Integer id;
  private String carType;
  private Byte automatic;
  private Integer noOfSeats;
  private String producer;
  private String model;
  private Integer graduationYear;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "car_type")
  public String getCarType() {
    return carType;
  }

  public void setCarType(String carType) {
    this.carType = carType;
  }

  @Basic
  @Column(name = "automatic")
  public Byte getAutomatic() {
    return automatic;
  }

  public void setAutomatic(Byte automatic) {
    this.automatic = automatic;
  }

  @Basic
  @Column(name = "no_of_seats")
  public Integer getNoOfSeats() {
    return noOfSeats;
  }

  public void setNoOfSeats(Integer noOfSeats) {
    this.noOfSeats = noOfSeats;
  }

  @Basic
  @Column(name = "producer")
  public String getProducer() {
    return producer;
  }

  public void setProducer(String producer) {
    this.producer = producer;
  }

  @Basic
  @Column(name = "model")
  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  @Basic
  @Column(name = "graduation_year")
  public Integer getGraduationYear() {
    return graduationYear;
  }

  public void setGraduationYear(Integer graduationYear) {
    this.graduationYear = graduationYear;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CarModelEntity that = (CarModelEntity) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (carType != null ? !carType.equals(that.carType) : that.carType != null) {
      return false;
    }
    if (automatic != null ? !automatic.equals(that.automatic) : that.automatic != null) {
      return false;
    }
    if (noOfSeats != null ? !noOfSeats.equals(that.noOfSeats) : that.noOfSeats != null) {
      return false;
    }
    if (producer != null ? !producer.equals(that.producer) : that.producer != null) {
      return false;
    }
    if (model != null ? !model.equals(that.model) : that.model != null) {
      return false;
    }
    if (graduationYear != null ? !graduationYear.equals(that.graduationYear)
        : that.graduationYear != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (carType != null ? carType.hashCode() : 0);
    result = 31 * result + (automatic != null ? automatic.hashCode() : 0);
    result = 31 * result + (noOfSeats != null ? noOfSeats.hashCode() : 0);
    result = 31 * result + (producer != null ? producer.hashCode() : 0);
    result = 31 * result + (model != null ? model.hashCode() : 0);
    result = 31 * result + (graduationYear != null ? graduationYear.hashCode() : 0);
    return result;
  }
}
