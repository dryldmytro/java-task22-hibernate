package com.dryl;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rent_list", schema = "MySixtDB")
public class RentListEntity {

  private Integer id;
  private Timestamp dateRentIn;
  private Timestamp dateRentOut;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "date_rent_in")
  public Timestamp getDateRentIn() {
    return dateRentIn;
  }

  public void setDateRentIn(Timestamp dateRentIn) {
    this.dateRentIn = dateRentIn;
  }

  @Basic
  @Column(name = "date_rent_out")
  public Timestamp getDateRentOut() {
    return dateRentOut;
  }

  public void setDateRentOut(Timestamp dateRentOut) {
    this.dateRentOut = dateRentOut;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RentListEntity that = (RentListEntity) o;

    if (id != null ? !id.equals(that.id) : that.id != null) {
      return false;
    }
    if (dateRentIn != null ? !dateRentIn.equals(that.dateRentIn) : that.dateRentIn != null) {
      return false;
    }
    if (dateRentOut != null ? !dateRentOut.equals(that.dateRentOut) : that.dateRentOut != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (dateRentIn != null ? dateRentIn.hashCode() : 0);
    result = 31 * result + (dateRentOut != null ? dateRentOut.hashCode() : 0);
    return result;
  }
}
