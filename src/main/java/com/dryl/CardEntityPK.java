package com.dryl;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;

public class CardEntityPK implements Serializable {

  private Integer cardNumber;
  private Integer userId;

  @Column(name = "card_number")
  @Id
  public Integer getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(Integer cardNumber) {
    this.cardNumber = cardNumber;
  }

  @Column(name = "user_id")
  @Id
  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CardEntityPK that = (CardEntityPK) o;

    if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null) {
      return false;
    }
    if (userId != null ? !userId.equals(that.userId) : that.userId != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = cardNumber != null ? cardNumber.hashCode() : 0;
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    return result;
  }
}
